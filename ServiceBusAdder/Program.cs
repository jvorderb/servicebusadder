﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.ServiceBus;
using Ecova.DaaS.SharedEntities.StandardBill.Model;
using System.Collections.Generic;

namespace ServiceBusAdder
{
    class Program
    {
        const string ServiceBusConnectionString = "Endpoint=sb://wfenamespacetest.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=aaHFWoNkm+BqOt1iB3HU7cpjGhW1n6ooQ3bBib3Ufgc=";
        const string QueueName = "incomingbills";
        static IQueueClient queueClient;

        static readonly StandardBill standardBill_invalidMeter = new StandardBill()
        {
            //AssociatedAccountId = agreement.AgreementId, //Don't think we actually need this.
            AssociatedClientId = "8e2eea45-4559-47ac-8a0e-3f9d0fddbef3",
            AssociatedProviderId = "cb843129-ce33-4f55-a257-e8c9655fb768",
            MinAccountNumber = "12345",
            Meters = new List<Meter>()
                {
                    new Meter()
                    {
                        MeterNumber = "999"
                    }
                }
        };

        static void Main(string[] args)
        {
            MainAsync().GetAwaiter().GetResult();
        }

        static async Task MainAsync()
        {
            queueClient = new QueueClient(ServiceBusConnectionString, QueueName);

            Console.WriteLine("======================================================");
            Console.WriteLine("Press ENTER key to exit after sending all the messages.");
            Console.WriteLine("======================================================");

            // Send messages.
            await SendMessageAsync();

            Console.ReadKey();

            await queueClient.CloseAsync();
        }

        static async Task SendMessageAsync()
        {
            try
            {
                string messageBody = Newtonsoft.Json.JsonConvert.SerializeObject(standardBill_invalidMeter, Newtonsoft.Json.Formatting.None);
                var message = new Message(Encoding.UTF8.GetBytes(messageBody));
                Console.WriteLine($"Sending message: {messageBody}");
                await queueClient.SendAsync(message);
            }
            catch (Exception exception)
            {
                Console.WriteLine($"{DateTime.Now} :: Exception: {exception.Message}");
            }
        }
    }
}
